using Toybox.WatchUi as UI;
using Toybox.Graphics as Gfx;
using Toybox.System;

class DataView {

    var xPos;
    var yPos;

    var dataX;
    var dataY;
    var labelX;
    var labelY;

    var label;
    var value;

    //var dcHeight;

    var font;

    function initialize(axPos, ayPos, aLabel, aFont) {
        xPos = axPos;
        yPos = ayPos;

        dataX = xPos + 1;
        dataY = yPos-17;
        labelX = xPos + 2;
        labelY = yPos - 3;

        label = aLabel;
        value = "";

        font = aFont;
    }

    function checkHeight(dc) {
        //if (null == dcHeight) {
            var height = dc.getHeight();

            if (height < 55) {
                dataY = yPos -8;
                labelY = yPos - 4;
            }
            else {
                dataY = yPos - 52;
                labelY = yPos - 44;
            }
       //     dcHeight = height;
       //}
    }


    function display(dc) {
        checkHeight(dc);
        displayValue(dc);
        displayLabel(dc);

    }

    function displayLabel(dc) {
         //dc.setColor( Gfx.COLOR_RED , Gfx.COLOR_TRANSPARENT);
        //for (var i = 0; i < label.length(); i++) {
        //   dc.drawText( labelX, labelY + (i*12), Gfx.FONT_XTINY, label.substring(i, i+1) , Gfx.TEXT_JUSTIFY_CENTER);
        //}
        dc.drawText( labelX, labelY, Gfx.FONT_MEDIUM, "" + label, Gfx.TEXT_JUSTIFY_LEFT);
    }

    function displayValue(dc) {
        //dc.setColor( Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.drawText( dataX, dataY, font, "" + value.format("%.2f"), Gfx.TEXT_JUSTIFY_LEFT);
    }


    function minutesToTime(minutes) {
        //System.print("minutes = " + minutes);

        var wholeHours = (minutes/60).toNumber();

        var wholeMinutes = (minutes - (wholeHours * 60)).toNumber();
        //System.print("wholeMinutes = " + wholeMinutes);

        var seconds = (60 * (minutes - wholeMinutes - (wholeHours * 60)));
        //System.println("seconds = " + seconds);

        var result = "";

        if (wholeHours > 0) {
            result += wholeHours + ":";
        }
        result += wholeMinutes.format("%02d") + ":" + seconds.format("%02d");

        return result;

    }


}

class TimeData extends DataView {

    function initialize(xPos, yPos, aLabel, aFont) {
        DataView.initialize(xPos, yPos, aLabel, aFont);
    }

    function display(dc) {
        checkHeight(dc);
        displayLabel(dc);

        var valueString;
        if (0 == value || null == value) {
            valueString = "00:00";
        }
        else {
            valueString = DataView.minutesToTime(value);
        }

        dc.drawText( dataX, dataY, font, "" + valueString, Gfx.TEXT_JUSTIFY_LEFT);
    }

}


