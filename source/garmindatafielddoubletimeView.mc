using Toybox.WatchUi as UI;
using Toybox.System;
using Toybox.Lang;
using Toybox.Graphics as Gfx;

class garmindatafielddoubletimeView extends UI.DataField {

    var time;
    var font;

    //! Set the label of the data field here.
    function initialize() {
        font = UI.loadResource(Rez.Fonts.Roboto110x12Font);
        //font = Gfx.FONT_NUMBER_THAI_HOT;
        time = new TimeData(0, 0, "TIME", font);
    }

    function compute(info) {
        var currentTime = info.timerTime;
        if (null == currentTime)
        {
            currentTime = 0.0;
        }
        // convert to minutes
        currentTime =  currentTime.toFloat() / 1000 / 60;
        time.value = currentTime;

        return 0;
    }

    function onUpdate(dc) {
        dc.setColor( Gfx.COLOR_WHITE, Gfx.COLOR_WHITE);
        dc.clear();
        dc.setColor( Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);

        time.display(dc);
        return true;
    }

}